# Purpose
We needed a server to drive the coordinating back-end for our android-app we developed during a project course. We had a course in elixir the period before and I had fallen in love with the language and the rest of the groups didn't complain to loudly about using it for our server.

The apps purpose was to make people able to create groups and in these groups give eachother assignments or missions if you will. The creation, use and control of these missions were suposed to be very open-ended and the plan was to simply give the user a toolbox of all the devices sensors, gps, maps and then the ability to combine different conditions for these with if-statements in a flow-chart kind of way. Or just to write the task in text and require evidence of compleetion in the form of a picture or text with manual confirmation. As you can imagine the mission creation did never get feature-finished. But we got a basic version of the app working. With the server in this repo as its back-end ofc.

# Libs used
https://github.com/michalmuskala/jason

# Contributers
## Server
https://github.com/cornelis-str
## The rest of the project team

